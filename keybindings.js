"use strict";
var label_to_key = {'up' : "up", 'down' : "down", 'left' : "left", 'right' : "right", 'a' : "interact", 'b': "cancel", 'select': "select", 'start': "start"};

if(window.socket) {
    //=========
    //SOCKET.IO
    //=========
    socket.on('controller_connect', function (data) {
        let controller = make_controller(data.id);
        GS.register_controller(controller);
    });

    socket.on('press', function (data) {
        console.log(data.id+" press "+data.label);
        var keys = GS.controllers_by_id[data.id].keys;
        keys[label_to_key[data.label]] = true;
    });
    socket.on('release', function(data) {
        console.log(data.id+" press "+data.label);
        for(var key in GS.controllers_by_id[data.id].keys) GS.controllers_by_id[data.id].keys[key] = false;
    });

    socket.on('select', function(data) {
        GS.party.filter(e => e.controller_id === data.id).forEach(e => e.controller_id = -1);
        //TODO: Change data.name to data.character_id
        GS.party[data.name].controller_id = data.id;
    });
} else {
    //===============
    //REGULAR BROWSER
    //===============
    GS.controllers_by_id[0] = make_controller(0); //@MagicNumbers
    var keycode_to_key = {87 : "up", 83 : "down", 65 : "left", 68 : "right"};
    canvas.addEventListener('keydown', function(event) {
        var keyCode = event.keyCode; 
        for (var k in keycode_to_key) 
            if (k == keyCode) GS.controllers_by_id[0].keys[keycode_to_key[k]] = true;
    });
    canvas.addEventListener("keyup", function(event) {
        var keyCode = event.keyCode; 
        for (var k in keycode_to_key) 
            if (k == keyCode) GS.controllers_by_id[0].keys[keycode_to_key[k]] = false;
    });
    GS.player_array[0].controller_id = 0;
}

