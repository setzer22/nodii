"use strict"; 

function lerp_position (entity, deltaTime) {
    let {origin, dest, interp_time} = entity; //Ensure API

    interp_time += deltaTime * 4; //@MagicNumbers
    let reached_dest = false;
    if (interp_time > 1) {
        interp_time = 1;
        reached_dest = true;  
    }
    let pos = vsum(origin, vscal(interp_time, vsub(dest, origin)));
    return [pos, interp_time, reached_dest];
}

function implement_interpolable (obj, pos) {
    obj.origin = pos || 0;
    obj.dest = pos || 0;
    obj.interp_time = 0;
    obj.new_dest_on_last_update = false;
}
