"use strict";
function make_battle_entity (spec) {
    let entity = {};

    if (spec.pos !== undefined) spec.pos = vfloor(vscal(1/32, spec.pos));

    entity.sprite = make_sprite();

    //Drawable
    Utils.proxy_property(entity.sprite, entity, "pos");

    //Entity
    entity.health = spec.health || 0;
    entity.wait_time = spec.wait_time || 5;
    entity.current_time = 0;
    entity.state = "waiting";

    entity.damage = function (amount) {
        entity.health -= amount;
        if (entity.health <= 0) entity.die();
    }

    entity.die = function () {
        entity.state = "dead";
        GS.action_queue.remove_player_actions(entity);
    }

    entity.is_dead = function () {
        return entity.state === "dead";
    }

    entity.on_select_action = function (action) {
        if (entity.state === "selecting_action") {
            //Select the action
            console.log('selectiong action '+action);
            entity.state = "action_selected";
            GS.action_queue.push(action);
        }
    }

    //TODO: Draw should be on player not on entity 
    entity.draw = function(viewport) {
        let sprite = entity.sprite;
        sprite.draw(viewport);
        viewport.context2d.font = "15pt Arial";
        with_style("#ffffff", viewport, () => {
            viewport.drawText(entity.health, sprite.pos.x, sprite.pos.y-50);
            viewport.drawText(Math.round(entity.current_time), sprite.pos.x, sprite.pos.y-20);
        });
        if (entity.state === "selecting_action") {
            with_style("#55ff55", viewport, () => viewport.drawRect(sprite.pos.x, sprite.pos.y, 10, 10));
        }
        else if (entity.state === "waiting") {
            with_style("#5555ff", viewport, () => viewport.drawRect(sprite.pos.x, sprite.pos.y, 10, 10));
        }
        else if (entity.state === "action_selected") {
            with_style("#222222", viewport, () => viewport.drawRect(sprite.pos.x, sprite.pos.y, 10, 10));
        }
        else if (entity.state === "TEST_attacking") {
            with_style("#ff0000", viewport, () => viewport.drawRect(sprite.pos.x, sprite.pos.y, 10, 10));
        }
        else if (entity.state === "dead") {
            with_style("#ffffff", viewport, () => viewport.drawRect(sprite.pos.x, sprite.pos.y, 10, 10));
        }
    }

    entity.update = function(timeStep) {
        if (entity.state === "waiting") {
            entity.current_time += timeStep;      
            if (entity.current_time > entity.wait_time) {
                entity.current_time = entity.wait_time; 
                entity.state = "selecting_action";
            }
        }
        else if (entity.state === "selecting_action") {
            let [selected, action] = entity.ia();
            if(selected) {
                entity.on_select_action(action);
            }
        }
        else if (entity.state === "action_selected") { 
        }
        else if (entity.additional_states && entity.additional_states[entity.state]) {
            entity.additional_states[entity.state](timeStep);
        }
    }

    return entity;

}
