"use strict";
function make_battle_enemy (spec) {
    let enemy = make_battle_entity(spec);

    Sprite.init_terra_sprite(
        enemy.sprite, 
        spec.pos || vector2(0,0),
        spec.name || ""
    );

    enemy.sprite.change_line(UtilityTables.directions_to_sprite_line["right"]); 

    let attack_action = {
        start: function () {
            enemy.TEST_countdown_timer = 3;
            console.log('enemy '+enemy.name+' attacks!');
            enemy.state = "TEST_attacking";
        }
    }

    enemy.ia = function () {
        return [true, attack_action];
    }

    //TODO: Draw should be on enemy not on entity 

    enemy.additional_states = {
        "TEST_attacking" : function (timeStep) {
            enemy.TEST_countdown_timer -= timeStep;
            if(enemy.TEST_countdown_timer < 0) {
                let inflicted_damage = Math.floor(Math.random()*10);
                console.log('enemy inflicts '+inflicted_damage+' points of damage');
                let selected = Math.random() > 0.5 ? 0 : 1;
                if (GS.battle_sprite_array[selected].state === "dead") selected = 1 - selected;
                GS.battle_sprite_array[selected].damage(inflicted_damage);
                GS.action_queue.on_action_performed();
                enemy.state = "waiting";
                enemy.current_time = 0;
            }
        }
    }

    return enemy;
}
