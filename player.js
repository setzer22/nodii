"use strict";

function make_player(spec) {
    let {pos, name} = spec;
    let player = {};

    //Sprite, TODO: Use spec
    player.sprite = make_sprite();
    Sprite.init_terra_sprite(player.sprite, pos, name);

    //Drawable
    Utils.proxy_property(player.sprite, player, "pos");
    Utils.proxy_function(player.sprite, player, "draw");

    //Interpolable
    implement_interpolable(player, player.sprite.pos);

    //Player
    if (spec.party_id === undefined) return Utils.error("No party id specified");
    player.party_id = spec.party_id;
    player.name = name;
    player.state = "controlled";
    player.change_state_to = "controlled";

    if(GS.tile_info[pos.y][pos.x].entity !== undefined) {
        console.log('Tried to create a character on an occupied position');
        return undefined;  
    }
    else {
        GS.tile_info[pos.y][pos.x].entity = player;
    }

    player.update = function (deltaTime) {

        let T = GS.tile_size;
        let sprite = player.sprite;

        let input = Utils.get_controller_of_party_member(player.party_id);
        if(!input) return;

        player.state = player.change_state_to;

        sprite.update(deltaTime); // Animation gets resetted every frame when stopped

        // To detect an interact key, the interact key has to have been unpressed some later frame
        if(!input.keys.interact) player.interact_unpressed = true;

        if(player.state === "controlled") {

            //TODO: Pure function or encapsulation?
            [sprite.pos, player.interp_time, player.new_dest_on_last_update] = lerp_position(player, deltaTime);

            if(player.new_dest_on_last_update) {

                //Reset the origin tile if we've reached the destination
                let {x:ox, y:oy} = Utils.pos_to_tile(player.origin);
                if(GS.tile_info[oy][ox].entity === player) {
                    GS.tile_info[oy][ox].entity = undefined;
                }

                let {directions_to_vector, directions_to_sprite_line, sprite_line_to_direction} = UtilityTables;

                let has_moved = false;
                let {x:tx, y:ty} = Utils.pos_to_tile(sprite.pos);

                let {x: lx, y: ly} = vsum(directions_to_vector[sprite_line_to_direction[sprite.animation_line]], vector2(tx, ty));
                if(player.interact_unpressed && input.keys.interact && GS.tile_info[ly][lx].entity && GS.tile_info[ly][lx].entity.on_interact) {
                    //TODO: I should distinguish between locking and non-locking interactions when I need to
                    let {entity} = GS.tile_info[ly][lx].entity.on_interact(player)
                    player.entity_interacting_with = entity;
                    player.interact_unpressed = false;
                    if (player.entity_interacting_with !== undefined) {
                        player.change_state_to = "interacting";
                    }
                }

                //Check every direction for keypress 
                for(var dir in directions_to_vector) {
                    let nx = tx + directions_to_vector[dir].x;
                    let ny = ty + directions_to_vector[dir].y;

                    if(Utils.in_bounds(GS.map, nx, ny) && input.keys[dir]) {
                        sprite.change_line(directions_to_sprite_line[dir]); 
                        let entity = GS.tile_info[ny][nx].entity;
                        if(GS.map.walkable(nx, ny) && (entity === player || entity === undefined)) {
                            has_moved = true;

                            player.interp_time = 0;
                            player.origin = sprite.pos;
                            player.dest = vsum(player.dest, vscal(T, directions_to_vector[dir]));
                            let {x:dx, y:dy} = Utils.pos_to_tile(player.dest);
                            GS.tile_info[dy][dx].entity = player;
                        }
                    }
                }
                if (!has_moved) {
                    sprite.reset_animation();
                }
            }
        }
        else if (player.state === "interacting") {
            sprite.reset_animation();
            if(player.interact_unpressed && input.keys.interact) {
                player.interact_unpressed = undefined;                
                let {interaction_finished} = player.entity_interacting_with.on_interact(player);
                if(interaction_finished) {
                    player.entity_interacting_with = undefined;
                    player.change_state_to = "controlled";
                }
            }
        }
    };

    return player;
}
