"use strict";
function make_battle_player (spec) {
    let player = make_battle_entity(spec);

    Sprite.init_terra_sprite(
        player.sprite, 
        spec.pos || vector2(0,0),
        spec.name || ""
    );

    if (spec.party_id === undefined) return Utils.error("No party id specified");
    player.party_id = spec.party_id;

    player.sprite.change_line(UtilityTables.directions_to_sprite_line["left"]); 

    let attack_action = {
        sender: player,
        start: function () {
            player.TEST_countdown_timer = 3;
            console.log('player '+player.name+' attacks!');
            player.state = "TEST_attacking";
        }
    }

    player.ia = function () {
        let controller = Utils.get_controller_of_party_member(player.party_id);
        //Ask for player action here... This should trigger the controller. For now we just wait for an A press
        if (controller && controller.keys.interact) {
            return [true, attack_action];
        }
        else {
            return [false, undefined];
        }
    }

    //TODO: Draw should be on player not on entity 

    player.additional_states = {
        "TEST_attacking" : function (timeStep) {
            player.TEST_countdown_timer -= timeStep;
            if(player.TEST_countdown_timer < 0) {
                let inflicted_damage = Math.floor(Math.random()*10);
                console.log('player inflicts '+inflicted_damage+' points of damage');
                GS.battle_sprite_array[2].damage(inflicted_damage);
                GS.action_queue.on_action_performed();
                player.state = "waiting";
                player.current_time = 0;
            }
        }
    }

    return player;
}
