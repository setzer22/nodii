"use strict";

var Utils = {

    // Converts a world position into a map position. Returned position might end up out of bounds
    pos_to_tile : function (pos) {
        return vector2(Math.floor(pos.x/GS.tile_size), Math.floor(pos.y/GS.tile_size));
    },

    // Returns an NxM matrix filled with copies of fill
    matrix: function (N, M, fill) {
        let array = [];
        for(let i = 0; i < N; ++i) {
            array[i] = [];
            for(let j = 0; j < N; ++j) {
                let obj = {};
                Object.assign(obj, fill);
                array[i][j] = obj;
            }
        }
        return array;
    },

    in_bounds: function (map, x, y) {return x >= 0 && x < map.width && y >= 0 && y < map.height;},

    udlr : function (a0, a1, a2, a3) {return {"up" : a0, "down" : a1, "left" : a2, "right" : a3};},

    proxy_property: function (original_object, proxy_object, property_name) {
        Object.defineProperty(proxy_object, property_name, {
            get: function () {return original_object[property_name];},
            set: function (property) {return original_object[property_name] = property;},
        });
    },

    proxy_function: function(original_object, proxy_object, function_name) {
        proxy_object[function_name] = original_object[function_name]; 
    },

    vector_to_direction: function (v) {
        //Known bug. When entities move this numbers are never 
        //exactly one
        if (v.y === -1) return "up";
        else if (v.y === 1) return "down";
        else if (v.x === -1) return "left";
        else if (v.x === 1) return "right";
        //if precondition is not met, return undefined
    },

    get_controller_of_party_member: function (party_id) {
        let c_id = GS.party[party_id].controller_id;
        if(c_id === -1) {
            return undefined;
        }
        let controller = GS.controllers_by_id[c_id];
        if(controller === undefined) {
            //TODO: Maybe at this point let the player's controller be unassigned?
            console.log ("requested for undefined controller with id "+c_id);
        }
        return controller;
    },

    error: function (msg) {
        console.log(msg);
    }
}

var UtilityTables = {
    directions_to_vector : Utils.udlr(vector2(0,-1), vector2(0,1), vector2(-1,0), vector2(1,0)),

    directions_to_sprite_line : Utils.udlr(0,1,2,3),

    sprite_line_to_direction : {0: "up", 1: "down", 2: "left", 3: "right"},
}

