function with_style(style, viewport, func) {
    if (viewport !== undefined) var canvas = viewport.context2d;
    let old_style = canvas.fillStyle;
    canvas.fillStyle = style;
    func.call();
    canvas.fillStyle = old_style;
}

