"use strict"; 

let DIALOG_BOX_HEIGHT = 4*35;

function make_dialog (spec) {
    let dialog = {
        pos: spec.pos || vector2(0,0),
        text: spec.text || "",
        line_width: spec.line_width || 300,
        current_page: 0,
    }

    function take_word(text, delimiter) {
        let i = 0; 
        while(i < text.length && text[i] !== delimiter) ++i;
        let ret = [text.substring(0, i), text.substring(i+1, text.length)];
        return ret;
    }

    dialog.next_page = function () {
        if(dialog.current_page + 1 === dialog.text.length) return false;
        ++dialog.current_page;
        return true;
    }

    function split_in_lines(line_width, text, context2d) {
        let lines = [""];
        let current_line_width = 0
        let current_line = 0; 
        let word = "";

        while(text !== "") {    
            [word, text] = take_word(text, " ");
            let word_width = context2d.measureText(word).width;
            //If the word is too big for a line, we put it in a single line to avoid an infinite loop
            if(current_line_width + word_width > dialog.line_width && lines[current_line].length >= 1) {
                current_line++;
                current_line_width = 0;
                lines.push("");
            }
            lines[current_line] += " " + word;
            current_line_width += word_width;
        }
        return lines;
    }

    dialog.draw = function (viewport) {
        let context = viewport.context2d;
        context.font = "bold 12pt Arial";
        let text = dialog.text[dialog.current_page];
        let lines = split_in_lines (dialog.line_width, text, context);
        context.globalAlpha = 0.2;
        context.fillStyle = "#222222";
        viewport.drawRect(dialog.pos.x - 10, dialog.pos.y - 35, dialog.line_width + 20, DIALOG_BOX_HEIGHT); //@MagicNumbers
        context.globalAlpha = 1.0;
        context.fillStyle = "#FFFFFF";
        lines.forEach((line, i) => viewport.drawText(line, dialog.pos.x, dialog.pos.y + 25*i, dialog.line_width));
    }

    dialog.show = function(GS) {
        GS.dialog_array.push(dialog);
    }

    dialog.hide = function(GS) {
        let index = GS.dialog_array.indexOf(dialog);
        GS.dialog_array.splice(index, 1);
    }

    dialog.on_accept = function (GS) {
        dialog.hide();    
    }

    return dialog;
}
