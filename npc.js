"use strict";

var NPC_Movement_Type = Object.freeze({
    FOLLOW_PATH: 0,
    RANDOM: 1,
    FOLLOW_PLAYER: 2,
});

function make_npc(spec) {
    let {pos, name} = spec;
    let npc = {};

    //Sprite
    npc.sprite = make_sprite(pos, name);
    Sprite.init_terra_sprite(npc.sprite, pos, name);

    //Drawable
    Utils.proxy_property(npc.sprite, npc, "pos");
    Utils.proxy_function(npc.sprite, npc, "draw");

    //Interpolable
    implement_interpolable(npc, npc.sprite.pos);

    //NPC
    npc.name = name;
    npc.path_index = 0;
    npc.path = ['up', 'up', 'left', 'down', 'down', 'right'];
    npc.movement_type = NPC_Movement_Type.RANDOM;
    npc.wait_time = Math.random() * 2;
    npc.wait_time_left = 0.0;
    npc.message = spec.message || "";
    npc.state = "controlled"

    function next_direction (npc) {
        let {movement_type} = npc;
        switch(movement_type) {
            case NPC_Movement_Type.FOLLOW_PATH:
                let dir = npc.path[npc.path_index];
                return dir;
            case NPC_Movement_Type.RANDOM:
                return ["up", "down", "left", "right"][Math.floor(Math.random() * 4)];
                break;
            case NPC_Movement_Type.FOLLOW_PLAYER:
                //TODO: This
                break;
        }
    }

    function direction_to (entity) {
        let entity_tile = Utils.pos_to_tile(entity.pos);
        let npc_tile = Utils.pos_to_tile(npc.pos);
        let delta_tile = vsub(entity_tile, npc_tile);
        return Utils.vector_to_direction(delta_tile);
    }

    function look_at (direction) {
        npc.sprite.change_line(UtilityTables.directions_to_sprite_line[direction]);
    }

    if(npc.message !== "") {
        npc.on_interact = function (interacting_player) {
            if (npc.state === "interacting" && npc.interacting_player !== interacting_player) return {entity: undefined};

            let interaction_finished = false;
            if(npc.dialog !== undefined) {
                if (!npc.dialog.next_page()) {
                    npc.dialog.hide(GS); 
                    npc.dialog = undefined;
                    npc.interacting_player = undefined;
                    npc.state = "controlled";
                    interaction_finished = true;
                }
            }
            else {
                let dialog_offset = vector2(-16, -DIALOG_BOX_HEIGHT + 10);
                npc.dialog = make_dialog({pos: npc.pos, text: npc.message});
                npc.dialog.pos = vsum(npc.dialog.pos, dialog_offset);
                npc.dialog.pos.x = Math.min(Math.max(0, npc.dialog.pos.x), GS.screen_width - 300)
                npc.dialog.pos.y = Math.min(Math.max(0, npc.dialog.pos.y), GS.screen_height - 300)
                console.log(npc.dialog.pos);

                npc.dialog.show(GS);
                npc.state = "interacting";
                npc.interacting_player = interacting_player;
                look_at(direction_to(interacting_player));
            }
            return {entity: npc, interaction_finished};
        }
    }

    npc.update = function (deltaTime) {
        //@CopyPaste - Player.update
        let T = GS.tile_size;

        [npc.pos, npc.interp_time, npc.new_dest_on_last_update] = lerp_position(npc, deltaTime);
        npc.sprite.update(deltaTime);

        //Even if we're interacting we have to clear the square we were. That's why this piece of code is outside the state if
        if(npc.new_dest_on_last_update) {
            let {x:ox, y:oy} = Utils.pos_to_tile(npc.origin);
            if(GS.tile_info[oy][ox].entity === npc) {
                GS.tile_info[oy][ox].entity = undefined;
            }
        }

        if(npc.state === "controlled") {
            if(npc.new_dest_on_last_update) {
                npc.wait_time_left -= deltaTime;
                if(npc.wait_time_left > 0) {
                    npc.sprite.reset_animation();
                }
                else {
                    let wait_variance = (npc.wait_time * 0.2);
                    npc.wait_time_left = npc.wait_time + Math.random() * wait_variance - wait_variance / 2; //@MagicNumbers
                    let {directions_to_vector, directions_to_sprite_line} = UtilityTables;

                    let dir = next_direction(npc);
                    npc.path_index = (npc.path_index + 1) % npc.path.length;
                    let {x:tx, y:ty} = Utils.pos_to_tile(npc.pos);

                    let nx = tx + directions_to_vector[dir].x;
                    let ny = ty + directions_to_vector[dir].y;
                    let success = false;

                    if(Utils.in_bounds(GS.map, nx, ny)) {
                        let entity = GS.tile_info[ny][nx].entity;
                        npc.sprite.change_line(directions_to_sprite_line[dir])
                        if(GS.map.walkable(nx, ny) && (entity === npc || entity === undefined)) {
                            success = true;
                            npc.interp_time = 0;

                            npc.origin = npc.pos;
                            npc.dest = vsum(npc.dest, vscal(T, directions_to_vector[dir]));

                            let {x:dx, y:dy} = Utils.pos_to_tile(npc.dest);
                            GS.tile_info[dy][dx].entity = npc;
                        }
                    }
                    if(!success) {
                        npc.sprite.reset_animation();
                        npc.path_index = npc.path_index - 1;
                        if(npc.path_index === -1) npc.path_index = npc.path.length - 1;
                    }
                }
            }
        }
        else if (npc.state === "interacting") {
            npc.sprite.reset_animation();
        }
    }

    return npc; 
}


