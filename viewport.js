"use strict"; 

function make_viewport (spec) {
    if(spec.context2d === undefined) return undefined;

    let context2d = spec.context2d;
    let camera = {
        pos: spec.camera_pos || vector2(0,0),
    };

    let viewport = {
        context2d,
        camera,
        width: context2d.canvas.width,
        height: context2d.canvas.height,
        active: spec.active || true,
    }

    viewport.drawRect = function (x, y, w, h) {
        context2d.fillRect(x + camera.pos.x, y + camera.pos.y, w, h);
    }

    viewport.drawText = function (text, x, y, width) {
        context2d.fillText(text, x + camera.pos.x, y + camera.pos.y, width);
    }

    viewport.drawImage = function (image, sx, sy, sw, sh, dx, dy, dw, dh) {
        context2d.drawImage(image, sx, sy, sw, sh, dx + camera.pos.x, dy + camera.pos.y, dw, dh);
    }

    return viewport;
}
