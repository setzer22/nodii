"use strict";

function make_sprite(spec) {
    if(spec === undefined) var spec = {};
    var sprite = {
        pos: spec.pos || vector2(0, 0), // Drawing position.
        center: spec.center || vector2(0, 0), // Drawing is done relative to the center point.
        size: spec.size || vector2(0, 0), // Drawn rectangle size, frames will be projected to sprite size
        source_size: spec.source_size || vector2(0, 0), // Source frame size, a frame takes sprite size in pixels.

        animation_line: spec.animation_line || 0,   // The line of the spritesheet that will be drawn.
        frame_count: spec.frame_count || 0,       // The number of frames per line.
        frame_index: spec.frame_index || 0,       // The current frame in the line.
        frame_duration: spec.frame_duration || 1,    // The duration of a frame.
        frame_time: spec.frame_time || 0,        // The elapsed seconds since the current frame started

        spritesheet: spec.spritesheet || undefined, // The spritesheet image
    };

    sprite.update = function (timeStep) {
        sprite.frame_time += timeStep;
        while (sprite.frame_time - sprite.frame_duration > 0) {
            sprite.frame_time -= sprite.frame_duration;
            sprite.frame_index = (sprite.frame_index + 1) % sprite.frame_count;
        }
    }

    sprite.draw = function (viewport) {
        if(sprite.spritesheet === undefined) {
            viewport.drawRect(sprite.pos.x - sprite.center.x ,sprite.pos.y -  sprite.center.y, sprite.size.x, sprite.size.y);
        }
        else {
            viewport.drawImage(sprite.spritesheet, 
                sprite.frame_index*sprite.source_size.x, sprite.animation_line*sprite.source_size.y, sprite.source_size.x, sprite.source_size.y, //Source rect
                sprite.pos.x - sprite.center.x , sprite.pos.y - sprite.center.y, sprite.size.x, sprite.size.y //Dest rect
            );
        }
    }

    sprite.change_line = function (line) {
        sprite.frame_time = 0;
        sprite.animation_line = line;
    }

    sprite.reset_animation = function () {
        sprite.frame_index = 0;
        sprite.frame_time  = 0;
    }

    return sprite;
}

var Sprite = {
    init_terra_sprite: function (sprite, pos, name) {
        let T = GS.tile_size;
        let grid_center = vector2(T/2, T/2);

        sprite.size = vector2(T, 2*T);
        sprite.source_size = vector2(T, 2*T);
        sprite.center = vector2(T/2, T + T/2);
        sprite.pos = vsum(vector2(pos.x * T,pos.y * T), grid_center);
        sprite.animation_line = 0;
        sprite.frame_count = 4;
        sprite.frame_duration = 0.15;
        let terra_img = new Image();
        terra_img.src = "res/terra_sprite.png";
        terra_img.onload = function() {
            sprite.spritesheet = terra_img;
        }
    },
};
