var MAX_PLAYER_INPUT_ID = 65536;
var min_virtual_controller_id = MAX_PLAYER_INPUT_ID;

function make_controller (id) {
    if(id >= MAX_PLAYER_INPUT_ID) {
        console.log("Player IDs can't get past 65536. Value is "+id);
        return undefined;
    }
    var input = {
        keys : {
            up : false,
            down : false,
            left : false,
            right : false
        },
        id : id,
    }
    return input;
}

function make_virtual_controller () {
    var input = {
        keys : {
            up : false,
            down : false,
            left : false,
            right : false
        },
        id : min_virtual_controller_id,
    }
    ++min_virtual_controller_id;
    return input;
}

var Input = {
    clear: function (input) {
        for (key in input) { input.keys[key] = false; }
    }
}
