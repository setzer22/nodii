"use strict";
function get_xml_async (path) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", path, false);
    xmlhttp.send();

    let parser = new DOMParser();

    return parser.parseFromString(xmlhttp.response, "application/xml");

}

function create_tmxMap(path) {
    var map = {};
    var xml_map = get_xml_async(path).childNodes[0];

    map.width = parseInt(xml_map.attributes.width.value);
    map.height = parseInt(xml_map.attributes.height.value);
    
    map.custom_tiles = {};
    map.tilesets = [];
    let tileset_list = xml_map.getElementsByTagName("tileset");

    let tileset_count = 0;
    for (let _tileset_i = 0; _tileset_i < tileset_list.length; ++_tileset_i) {
        let tileset = tileset_list[_tileset_i];
        let tileset_obj = {};

        // The firstgid can't be stored externally, it's always on the original tileset tag
        tileset_obj.firstgid = parseInt(tileset.attributes.firstgid.value);

        // Check if tileset is sepparate somewhere or is embedded inline
        if (tileset.attributes.source !== undefined) {
            tileset = get_xml_async(tileset.attributes.source.value).childNodes[0];
        }

        // TODO: This should be done only once, only one tile dimension is allowed.
        map.tileWidth = parseInt(tileset.attributes.tilewidth.value);
        map.tileHeight = parseInt(tileset.attributes.tileheight.value);

       // Get the image data for the tileset
        var image = tileset.getElementsByTagName("image")[0];
        tileset_obj.tilesetWidth = parseInt(image.attributes.width.value) / map.tileWidth;
        tileset_obj.tilesetHeight = parseInt(image.attributes.height.value) / map.tileHeight; 
        tileset_obj.img_source = image.attributes.source.value;

        tileset_obj.img = new Image();
        tileset_obj.img.onload = function () {tileset_obj.img.loaded = true;};
        tileset_obj.img.src = tileset_obj.img_source;

        map.tilesets[tileset_count] = tileset_obj;
        tileset_count = tileset_count + 1;

        // Go through the tileset's tiles looking for custom properties
        var tile_nodes = tileset.getElementsByTagName("tile");
        var i;
        if(tile_nodes.length > 0) {
            for (i = 0; i < tile_nodes.length; ++i) {
                var tile = tile_nodes[i];
                var properties = {};

                if(tile.getElementsByTagName("properties").length > 0) {
                    var properties_xml = tile.getElementsByTagName("properties")[0].getElementsByTagName("property");
                    for(var j = 0; j < properties_xml.length; ++j) {
                        var property = properties_xml[j];
                        properties[property.attributes.name.value] = property.attributes.value.value;
                    }
                }

                map.custom_tiles[parseInt(tile.attributes.id.value)] = properties;
            }
        }
    }

    map.layers = [];

    var layers = xml_map.getElementsByTagName("layer");
    for (var i = 0; i < layers.length; ++i) {
        var layerData = [];
        var tiles = layers[i].getElementsByTagName("data")[0].getElementsByTagName("tile");
        for(var j = 0; j < tiles.length; ++j) {
            layerData[j] = parseInt(tiles[j].attributes.gid.value);
        }

        if(layers[i].attributes.name.value == "collision") {
            map.collisionLayer = layerData;
        }
        else {
            map.layers[i] = {};
            map.layers[i].data = layerData;
            map.layers[i].name = layers[i].attributes.name.value;
            //TODO: Find out a good convention for this
            map.layers[i].over_player = map.layers[i].name.indexOf("Over_Player") === 0;
        }
    }

    populate_map_functions(map);
    
    console.log(map);
    return map;
}

//pre: - map returned by create_tmxMap
function render_tmxMap(map, viewport, draw_under_layers) {
    //TODO: game has to call over and under at different times, this can't be handled here
    if(draw_under_layers === undefined) draw_under_layers = true;
    let over_layers = map.layers.filter(l => l.over_player);
    let under_layers = map.layers.filter(l => !l.over_player);

    console.log(over_layers);
    console.log(under_layers);

    function render (layer) {
        var data = layer.data,
            tilesets = map.tilesets,
            tw = map.tileWidth,
            th = map.tileHeight;

        if (!map.tilesets.some(x => x.img.loaded)) return;
        //if (!map.img.loaded) return;

        for(var i = 0; i < map.width; ++i) {
            for(var j = 0; j < map.height; ++j) {
                if(data[i*map.width+j] == 0) continue;

                let gid = data[i*map.width+j];
                let tileset_index = 0;

                while (tileset_index < map.tilesets.length && gid >= map.tilesets[tileset_index].firstgid) {
                    tileset_index += 1;
                }
                tileset_index -= 1;
                //post: min_tid_i-1 <= gid <= min_tid_i

                let tid = gid - map.tilesets[tileset_index].firstgid + 1;
                let tileset_w = map.tilesets[tileset_index].tilesetWidth;
                let tileset_h = map.tilesets[tileset_index].tilesetHeight;

                var x0 = tw*Math.floor((tid-1) % tileset_w);
                var y0 = th*Math.floor((tid-1) / tileset_h);
                viewport.drawImage(map.tilesets[tileset_index].img, x0, y0, tw, th, j*tw, i*th, tw, th);
            }
        }
    }

    if(draw_under_layers) {
        under_layers.forEach(l => render(l)); 
    }
    else {
        over_layers.forEach(l => render(l)); 
    }
}

function populate_map_functions(map) {
    map.walkable = function(x, y) {
        return map.collisionLayer[y * map.width + x] == 0;
    }
}
