var resource = {
    path : "/res",
    cache : {},

    loadImage : function(callback) {
        var image = new Image();
        if (typeof(callback) === "undefined") {
            image.onLoad = callback;
        }
        image.src = "/res/test.png";
    }
};
