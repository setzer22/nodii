"use strict";
function make_background (spec) {
    if (!spec.path) return undefined;

    let background = {};

    background.img = new Image();
    background.img.onload = function () {background.img_loaded = true;};
    background.img.src = spec.path;

    background.draw = function (viewport) {
        if (!background.img_loaded) return;
        viewport.drawImage(background.img,   0, 0, background.img.width, background.img.height,   0, 0, viewport.width, viewport.height);
    }

    console.log(background);
    return background;
}
