"use strict"; 

function vector2(x_, y_) {
    return {x: x_, y: y_};
}

function vsum(v1, v2) {
    return vector2(v1.x + v2.x, v1.y + v2.y);
}

function vsub(v1, v2) {
    return vector2(v1.x - v2.x, v1.y - v2.y);
}

function vdot(v1, v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

function vscal(x, v) {
    return vector2(x * v.x, x * v.y);
}

function vfloor(v) {
    return vector2(Math.floor(v.x), Math.floor(v.y));
}


