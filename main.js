"use strict";
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');

// ===========
//   ROUTING
// ===========
app.get('/controller', function(req, res) {
    console.log('b');
    res.sendfile('controller.html');
});

app.get('/new_controller', function(req, res) {
    console.log('b');
    res.sendfile('new_controller.html');
});

app.get('/game', function(req, res) {
    res.sendfile('game.html');
});

app.get(/^\/(.+\.(js|png))/, function(req, res) {
    console.log('requested '+req.params[0]);
    res.sendfile(req.params[0]);
});

app.get(/^\/(.+\.(tmx|xml|tsx))/, function(req, res) {
    console.log('requested xml '+req.params[0]);
    res.set('Content-Type', 'text/xml');
    res.sendfile(req.params[0]);
});


// ===============
//  GAME PROTOCOL
// ===============

var game_socket = null;
var new_controller_id = 0;

function deep_toString (obj) {
    var s = "{"
    for (var k in obj) {
        s += k + ":" + obj[k] + ", "; 
    }
    s += "}";
    return s;
}

var pending_controllers = [];

io.on('connection', function(socket) {
    console.log('connection');
    socket.on('identify', function(data) {
        if(data.client_type == 'game') {
            game_socket = socket;
            pending_controllers.forEach(function(_id) {
                game_socket.emit('controller_connect', {id:_id});
            });
        }
        else if (data.client_type == 'controller') {
            // I don't know if it's possible two controllers might end up getting the same id,
            // I should check on node's concurrency mechanisms... This shouldn't be too bad though
            console.log('sending id back '+new_controller_id);
            socket.emit('identify', {id : new_controller_id});
            if(game_socket != null) {
                game_socket.emit('controller_connect', {id: new_controller_id});
            } else {
                pending_controllers.push(new_controller_id);
            }
            new_controller_id = new_controller_id + 1;
        }
    });

    function forward_event_to_game(event_name) {
        socket.on(event_name, function(data) {
            console.log('EVENT '+event_name+' '+deep_toString(data));
            if(game_socket != null) {
                game_socket.emit(event_name, data);
            }
            else {
                console.log("... Can't reach game");
            }
        });
    }

    forward_event_to_game('press');
    forward_event_to_game('release');
    forward_event_to_game('select');
});


http.listen(7777, function() {
    console.log('listening on port 7777');
});
